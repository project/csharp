

CSharp filter
---------------------
by Zhongzf, zhongzf@126.com



Overview
--------
  The CSharp filter module adds the ability to include C# code in posts. C# 
is a multi-paradigm programming language encompassing imperative, declarative, 
functional, generic, object-oriented (class-based), and component-oriented 
programming disciplines. It was developed by Microsoft within the .NET 
initiative and later approved as a standard by Ecma (ECMA-334) and 
ISO (ISO/IEC 23270). C# is one of the programming languages designed for the 
Common Language Infrastructure;

  Through the CSharp filter, users with the proper permission may include 
custom C# code within a page of the site. While this is a powerful and flexible 
feature if used by a trusted user with C# experience, it is a significant and 
dangerous security risk in the hands of a malicious user. Even a trusted user 
may accidentally compromise the site by entering malformed or incorrect C# 
code. Only the most trusted users should be granted permission to use the 
CSharp filter, and all C# code added through the CSharp filter should be 
carefully examined before use.



Installation
-------------
1. csharp is required, 
it can be done with command "apt-get install mono-csharp-shell" under Ubuntu.



Documentation
-------------
The usage of csharp can be found on http://www.mono-project.com/CsharpRepl.
